package Domain.Test;

import org.junit.Test;
import static org.junit.Assert.*;

public class BankTest {
  @Test
  public void depositTest() {
    Bank bank = new Bank();
    bank.deposit(100);
    assertEquals(100, bank.getBalance());
  }

  @Test
  public void withdrawalTest() {
    Bank bank = new Bank();
    bank.deposit(100);
    bank.withdraw(50);
    assertEquals(50, bank.getBalance());
  }
}