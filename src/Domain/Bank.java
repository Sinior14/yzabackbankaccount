package Domain;


import java.util.ArrayList;

import Bean.Transaction;


public class Bank {
  private int balance;
  private ArrayList<Transaction> transactions;

  public Bank() {
    this.balance = 0;
    this.transactions = new ArrayList<>();
  }

  public void deposit(int amount) {
    balance += amount;
    transactions.add(new Transaction(amount, balance));
  }

  public void withdraw(int amount) {
    if (amount <= balance) {
      balance -= amount;
      transactions.add(new Transaction(-amount, balance));
    }
  }

  public int getBalance() {
    return balance;
  }

  public ArrayList<Transaction> getStatement() {
    return transactions;
  }
}