package Adapter;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import Bean.Transaction;

public class BankSQLiteAdapter {
  private Connection connection;

  public BankSQLiteAdapter() {
    try {
      connection = DriverManager.getConnection("jdbc:sqlite:bank.db");
      createTable();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  private void createTable() throws SQLException {
    Statement statement = connection.createStatement();
    statement.execute("CREATE TABLE IF NOT EXISTS transactions (id INTEGER PRIMARY KEY AUTOINCREMENT, date TEXT, amount INTEGER, balance INTEGER)");
    statement.close();
  }

  public void saveTransaction(Transaction transaction) throws SQLException {
    PreparedStatement statement = connection.prepareStatement("INSERT INTO transactions (date, amount, balance) VALUES (?, ?, ?)");
    statement.setString(1, transaction.getDate().toString());
    statement.setInt(2, transaction.getAmount());
    statement.setInt(3, transaction.getBalance());
    statement.executeUpdate();
    statement.close();
  }

  public ArrayList<Transaction> getTransactions() throws SQLException {
    ArrayList<Transaction> transactions = new ArrayList<>();
    Statement statement = connection.createStatement();
    ResultSet resultSet = statement.executeQuery("SELECT * FROM transactions");
    while (resultSet.next()) {
      transactions.add(new Transaction(resultSet.getInt("amount"), resultSet.getInt("balance"), new Date(resultSet.getString("date"))));
    }
    statement.close();
    return transactions;
  }
}