package Services;

import java.sql.SQLException;
import java.util.ArrayList;

import Adapter.BankSQLiteAdapter;
import Bean.Transaction;
import Domain.Bank;

public class BankService {
  private BankSQLiteAdapter bankSQLiteAdapter;
  private Bank bank;

  public BankService() {
    this.bankSQLiteAdapter = new BankSQLiteAdapter();
    this.bank = new Bank();
  }

  public void deposit(int amount) {
    bank.deposit(amount);
    try {
      bankSQLiteAdapter.saveTransaction(new Transaction(amount, bank.getBalance()));
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public void withdraw(int amount) {
    bank.withdraw(amount);
    try {
      bankSQLiteAdapter.saveTransaction(new Transaction(-amount, bank.getBalance()));
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public ArrayList<Transaction> getStatement() {
    ArrayList<Transaction> statement = new ArrayList<>();
    try {
      statement.addAll(bankSQLiteAdapter.getTransactions());
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return statement;
  }
}