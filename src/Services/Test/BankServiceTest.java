package Services.Test;

import org.junit.Test;
import static org.junit.Assert.*;

public class BankServiceTest {

  @Test
  public void depositTest() {
    BankService bankService = new BankService();
    bankService.deposit(100);
    assertEquals(100, bankService.getBalance());
  }

  @Test
  public void withdrawalTest() {
    BankService bankService = new BankService();
    bankService.deposit(100);
    bankService.withdraw(50);
    assertEquals(50, bankService.getBalance());
  }

  @Test
  public void getStatementTest() {
    BankService bankService = new BankService();
    bankService.deposit(100);
    bankService.withdraw(50);
    ArrayList<Transaction> statement = bankService.getStatement();
    assertEquals(2, statement.size());
    assertEquals(100, statement.get(0).getAmount());
    assertEquals(-50, statement.get(1).getAmount());
  }
}
